# Vue App with Python Apis
## Ambiente de Desenvolvimento

### Dependências
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

#### Api Auth (api-auth)
Serviço de autenticação que gera o token para o usuário.

###### Python
 ```
 mkvirtualenv -p /usr/bin/python3.6 api-auth
 pip3 install -r requirements.txt 
 python3 api.py
 ```
###### Docker
  ```
 docker-compose up
 ```
###### Testes
```
coverage run --source=. -m unittest discover -s test
```

#### Api User (api-user)
Serviço para gerenciamento de informações de usuário. É necessário possuir um token.

###### Python
 ```
 mkvirtualenv -p /usr/bin/python3.6 api-user
 pip3 install -r requirements.txt 
 python3 api.py
 ```
###### Docker
 ```
 docker-compose up
 ```
###### Testes
```
coverage run --source=. -m unittest discover -s test
```
