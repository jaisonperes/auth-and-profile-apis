Cerberus==0.9.2
click==6.7
coverage==4.4.2
blinker==1.4
deepdiff==3.3.0
eve-healthcheck==0.2.0
Eve==0.7.5
Events==0.2.2
Flask-Bcrypt==0.7.1
Flask-Cors==3.0.6
Flask-PyMongo==0.5.1
Flask==0.12
gunicorn==19.9.0
Jinja2==2.10
MarkupSafe==0.23
numpy>=1.14.5
pylint==1.8.2
pymongo==3.6.0
python-dotenv==0.6.5
requests==2.18.4
scipy>=1.1.0
sentry-sdk==0.7.6
simplejson==3.13.2