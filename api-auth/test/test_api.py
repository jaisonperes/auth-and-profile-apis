import unittest
from datetime import datetime
from eve import Eve
import os
from api import app
import settings
from base64 import b64encode
from os import environ, path
from pymongo.errors import ServerSelectionTimeoutError
import json

try:
    load_dotenv(find_dotenv())
except Exception as e:
    pass


class basicEveTest(unittest.TestCase):
    """Basic Eve test class
    It should let a client available and a app.
    Make requests with:

    self.test_client.get('/endpoit')

    """

    def setUp(self):
        dir_path = path.dirname(path.realpath(__file__))
        # self.app = Eve(settings=dir_path+'/../settings.py')
        self.app = app
        self.test_client = self.app.test_client()
        hash = bytes(environ.get("MONGO_USER") + ':' +
                     environ.get("MONGO_PASS"), "utf-8")
        self.headers = {
            'Authorization': 'Basic %s' % b64encode(hash).decode("ascii")
        }
        valid_user = {
            'email': 'teste@domain.com',
            'name': 'Teste',
            'surname': 'Teste',
            'nickname': 'teste',
            'cpf': '12323434545',
            'phone': '12234562345',
            'auth': {
                'password': '12323434545'
            }
        }
        with self.app.app_context():
            self.headers['Content-Type'] = 'application/json'
            self.post('/user/admin', json.dumps(valid_user))
            self.password = valid_user['auth']['password']
            self.valid_user = self.app.data.pymongo(
            ).db['users'].find_one({'nickname': valid_user.get('nickname')})

    def tearDown(self):
        with self.app.app_context():
            self.app.data.pymongo().db['users'].delete_one({'nickname': self.valid_user['nickname']})

    def get(self, url):
        try:
            response = self.test_client.get(url, headers=self.headers)
            return response
        except ServerSelectionTimeoutError as e:
            self.skipTest(str(response.response))

    def post(self, url, data):
        try:
            response = self.test_client.post(url, headers=self.headers, data=data)
            return response
        except ServerSelectionTimeoutError as e:
            self.skipTest(str(response.response))

if __name__ == '__main__':
    unittest.main(verbosity=3)
