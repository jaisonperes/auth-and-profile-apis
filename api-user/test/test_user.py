import json
from test_api import basicEveTest


class TestUserAPI(basicEveTest):

    """Basic User endpoint test

    Should test the basic endpoint of the user
    """

    def test_users(self):
        response = self.get('/user/admin')
        self.assertEqual(response.status_code, 200)
        response = self.get('/user/admin?where={"nickname":{"$exists":0}}')
        self.assertEqual(response.status_code, 200)
        items = json.loads(response.data.decode("utf-8"))['_items']
        self.assertTrue(len(items) == 0)

    def test_update_user(self):
        self.valid_user = json.loads(
            self.get('/user/admin/{}'.format(self.valid_user['_id'])).data.decode("utf-8"))
        self.headers['If-Match'] = self.valid_user['_etag']
        self.valid_user.pop('_created')
        self.valid_user.pop('_updated')
        self.valid_user.pop('_links')
        self.valid_user.pop('_etag')
        self.valid_user.pop('_deleted')
        id = self.valid_user.pop('_id')
        self.valid_user['nickname'] = 'teste'
        response = self.test_client.patch(
            '/user/admin/{}'.format(id), headers=self.headers, data=json.dumps(self.valid_user))
        self.assertEqual(response.status_code, 200, response.data)
    
    def test_without_header(self):
        response = self.get('/user/admin?where={"nickname":{"$exists":1}}', {})
        self.assertEqual(response.status_code, 401)
