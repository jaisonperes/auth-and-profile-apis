# Critérios de Avaliação

## Requisitos

- [x] Cadastro

    - [x] Nome
    - [x] Sobrenome
    - [x] E-mail
    - [x] Nome de usuário
    - [x] CPF (plus)
    - [x] Telefone (plus)
    - [x] Senha
    - [x] Validação de campos
    - [x] Validação de resposta da API

- [x] Login

    - [x] Nome de usuário
    - [x] Senha
    - [x] Mensagens de falhas sem informações sensíveis
    - [x] Validação de campos
    - [x] Validação de resposta da API

- [x] Acesso Ao Perfil

    - [x] Nome
    - [x] Sobrenome
    - [x] E-mail
    - [x] Nome de Usuário
    - [x] CPF (plus)
    - [x] Telefone (plus)
    - [x] Validação de resposta da API

- [x] Alteração de Perfil

    - [x] Nome
    - [x] Sobrenome
    - [x] E-mail
    - [x] Telefone (plus)
    - [x] Validação de campos
    - [x] Validação de resposta da API

### Avaliação

Em termos de requisitos, a implementação satisfaz o que foi proposto para desenvolvimento.

Entretanto, a implementação não atendeu critérios óbvios como validação de CPF e telefone. A implementação também não respeitou restrições implícitas, como a capacidade de modificar senha, cpf e nome de usuário quando o requisito E01F02 não determinou que fosse possível modificar senha, cpf e nome de usuário. Somado com o fato de ser possível limpar o perfil, usuário pode acabar perdendo a própria conta.

Mesmo ausente nos requisitos, a implementação verifica a unicidade de CPF, nome de usuário e e-mail, o que é válido e coerente com o contexto.

Por fim, a implementação não valida se campos obrigatórios estão vazios antes de enviar para o _backend_, algo que impediria vários problemas citados na seção **Cons** abaixo.

#### Prós

- Todas as _features_ foram implementadas
- Estabeleceu páginas para futuras _features_
- Verificou unicidade de campos que naturalmente devem ser únicos

#### Cons

- Não há validação de CPF e Telefone
- É possível modificar usuário, CPF e senha (requisito não consta que deva ser possível modificar tais campos)
- É possível enviar cadastro vazio (_backend_ retorna erro, que é tratado)
- É possível limpar todo o perfil
- Erro de campo com valor não-único não tratado devidamente
- Condicional que verifica se usuário existe na _topbar_ é incorreta (verifica se nome existe, não se usuário existe)
- Ao alterar (sem salvar) nome, também se altera nome do usuário atual (pode ser visto na interface, pelo nome do usuário na _topbar_)
- Menu responsivo não verifica estado do usuário (disponibiliza menu de _guest_ sempre)
- Realizar _refresh_ na página perde sessão de usuário.

## Frontend

- [x] Arquitetura em Módulos
- [x] Interface em Componentes
- [x] Componentes Reutilizáveis (plus)
- [x] Uso de SCSS/SASS e/ou Stylus
- [x] Responsividade para dispositivos móveis
- [x] Tratamento de _input_
- [x] Requisição para API
- [x] Tratamento de resposta da API
- [x] Sessão de usuário
- [x] Uso de Pug (plus)
- [x] Uso de Webpack (plus)
- [x] Uso de TypeScript, CoffeeScript e/ou ECMAScript 2018 (plus)
- [x] NÃO utilizar _frameworks_ de interface, como Bootstrap, Materialize e outros (plus)
- [x] Máscara de _input_ (plus)
- [x] Seguir padrão de design de mercado, como Material Design, Flat Design, Fluent Design e outros (plus)

### Avaliação

Percebe-se experiência com o _framework_ escolhido, seguindo corretamente o guia de estilo e composição de diretórios do **Vue**.

#### Prós

- Código bem escrito
- Interface segue devidamente o Material Design com pequenas características do Flat Design e animações agradáveis

#### Cons

- Parte dos **Cons** citados nos **Requisitos** são erros de implementação no _frontend_

## Backend

- [x] Arquitetura em Módulos
- [x] API em JSON
- [x] Permissão de acesso para endpoints da API via token, recebido no ato de login

### Avaliação

_Backend_ implementado de forma simplista, condizente com a característica do contexto.

A abordagem de estabelecer a arquitetura decentralizada é um ponto positivo, este que foge dos critérios de avaliação.

A definição de um contexto `admin` no _backend_ foge das definições nos requisitos. Nenhuma das _features_ eram destinadas à administradores.

#### Cons

- Parte dos **Cons** citados nos **Requisitos** são erros que poderiam ter sido identificados e impedidos no _backend_

## Infra/Projeto

- [x] Docker e docker-compose para subir instância de desenvolvimento (plus)
- [x] Redigir instruções no README.md para subir sistema (plus)
- [ ] Testes (plus)

### Avaliação

No ato da entrega, o sistema possuia um ambiente **docker** inutilizável. Seguir as instruções do `README.md` levava à serviços quebrados e conflituosos.

Entretanto, com um aditivo de 24 horas, tais problemas foram sanados.

A ausência de um ambiente adequado de testes prejudica o desenvolvimento.

#### Prós

- Desenvolvedor escolheu resolver os problemas de **docker** identificados, e assim o fez

#### Cons

- Testes de _backend_ falham
- Não há testes de _frontend_
